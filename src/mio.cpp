#include "mio.h"

#include <iterator>
#include <memory>
#include <sstream>
#include "mat5.h"
#include "mtraits.h"
#include "mtypes.h"

namespace std {
template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
}  // namespace std

namespace mio {

/* The polymorphic base class
 */
class marray_impl_base {
  public:
    virtual bool is_numeric_array() const { return false; }
    virtual bool is_struct_array() const { return false; }
    virtual bool is_cell_array() const { return false; }

    virtual mnumeric_array& as_numeric_array() { throw std::bad_cast(); }
    virtual const mnumeric_array& as_numeric_array() const
    {
        throw std::bad_cast();
    }

    virtual mstruct_array& as_struct_array() { throw std::bad_cast(); }
    virtual const mstruct_array& as_struct_array() const
    {
        throw std::bad_cast();
    }

    virtual mcell_array& as_cell_array() { throw std::bad_cast(); }
    virtual const mcell_array& as_cell_array() const { throw std::bad_cast(); }

    virtual std::size_t rows() const = 0;
    virtual std::size_t cols() const = 0;
    virtual const std::vector<std::size_t>& dims() const = 0;
    virtual std::ostream& stream(std::ostream&) const = 0;
    virtual std::string pt() const = 0;

    virtual MAT_CLASS mat_class() const = 0;

    virtual ~marray_impl_base() = default;
};

class marray_impl_numeric : public marray_impl_base {
  public:
    bool is_numeric_array() const override { return true; }

    mnumeric_array& as_numeric_array() override { return data; }
    const mnumeric_array& as_numeric_array() const override { return data; }

    std::size_t rows() const override { return data.rows(); }
    std::size_t cols() const override { return data.cols(); }
    const std::vector<std::size_t>& dims() const override
    {
        return data.dims();
    }
    std::ostream& stream(std::ostream& os) const override { return os << data; }
    std::string pt() const override { return data.pt(); }

    MAT_CLASS mat_class() const override { return data.mat_class(); }

    explicit marray_impl_numeric(mnumeric_array data) : data(std::move(data)) {}

    mnumeric_array data;
};

class marray_impl_struct : public marray_impl_base {
  public:
    bool is_struct_array() const override { return true; }

    mstruct_array& as_struct_array() override { return data; }
    const mstruct_array& as_struct_array() const override { return data; }

    std::size_t rows() const override { return data.rows(); }
    std::size_t cols() const override { return data.cols(); }
    const std::vector<std::size_t>& dims() const override
    {
        return data.dims();
    }
    std::ostream& stream(std::ostream& os) const override { return os << data; }
    std::string pt() const override { return data.pt(); }

    MAT_CLASS mat_class() const override { return data.mat_class(); }

    explicit marray_impl_struct(mstruct_array data) : data(std::move(data)) {}

    mstruct_array data;
};

class marray_impl_cell : public marray_impl_base {
  public:
    bool is_cell_array() const override { return true; }

    mcell_array& as_cell_array() override { return data; }
    const mcell_array& as_cell_array() const override { return data; }

    std::size_t rows() const override { return data.rows(); }
    std::size_t cols() const override { return data.cols(); }
    const std::vector<std::size_t>& dims() const override
    {
        return data.dims();
    }
    std::ostream& stream(std::ostream& os) const override { return os << data; }
    std::string pt() const override { return data.pt(); }

    MAT_CLASS mat_class() const override { return data.mat_class(); }

    explicit marray_impl_cell(mcell_array data) : data(std::move(data)) {}

    mcell_array data;
};

bool marray::is_numeric_array() const
{
    return !!impl && impl->is_numeric_array();
}
bool marray::is_struct_array() const
{
    return !!impl && impl->is_struct_array();
}
bool marray::is_cell_array() const { return !!impl && impl->is_cell_array(); }
bool marray::is_empty() const { return !impl; }

const mnumeric_array& marray::as_numeric_array() const
{
    if (!impl) throw std::bad_cast();
    return impl->as_numeric_array();
}
const mstruct_array& marray::as_struct_array() const
{
    if (!impl) throw std::bad_cast();
    return impl->as_struct_array();
}
const mcell_array& marray::as_cell_array() const
{
    if (!impl) throw std::bad_cast();
    return impl->as_cell_array();
}
mnumeric_array& marray::as_numeric_array()
{
    if (!impl) throw std::bad_cast();
    return impl->as_numeric_array();
}
mstruct_array& marray::as_struct_array()
{
    if (!impl) throw std::bad_cast();
    return impl->as_struct_array();
}
mcell_array& marray::as_cell_array()
{
    if (!impl) throw std::bad_cast();
    return impl->as_cell_array();
}

marray::marray(mnumeric_array data) : ndim(data.dims())
{
    impl = std::make_unique<marray_impl_numeric>(std::move(data));
}
marray::marray(mstruct_array data) : ndim(data.dims())
{
    impl = std::make_unique<marray_impl_struct>(std::move(data));
}
marray::marray(mcell_array data) : ndim(data.dims())
{
    impl = std::make_unique<marray_impl_cell>(std::move(data));
}

marray::~marray() = default;
marray::marray() = default;
marray::marray(marray&&) = default;
marray& marray::operator=(marray&&) = default;

void marray::set(std::string fn, marray&& v)
{
    as_struct_array().at(0).merge(mstruct(std::move(fn), std::move(v)));
}

mstruct::mstruct(std::string fn, marray&& data)
    : mstruct(std::make_pair(std::move(fn), std::move(data)))
{
}

mstruct::mstruct(std::pair<std::string, marray>&& nvp)
// : fields({{std::move(fn), std::move(data)}})
// unfortunately one can't move data with initializer lists
{
    fields[std::move(nvp.first)] = std::move(nvp.second);
}
marray& mstruct::field(const std::string& fn) { return fields.at(fn); }

const marray& mstruct::field(const std::string& fn) const
{
    return fields.at(fn);
}
bool mstruct::has_field(const std::string& fn) const { return fields.count(fn); }

marray mstruct::drop(const std::string& fn)
{
    marray ret = std::move(fields.at(fn));
    fields.erase(fn);
    return ret;
}

void mstruct::merge(mstruct&& other)
{
    for (auto& nvp : other.fields) {
        if (fields.find(nvp.first) != fields.end())
            throw std::runtime_error("can't merge overlapping structs: " +
                                     nvp.first);
        fields[nvp.first] = std::move(nvp.second);
    }
}

void mstruct::set(std::string fn, marray&& data)
{
    fields[std::move(fn)] = std::move(data);
}

std::ostream& operator<<(std::ostream& os, const mstruct& s)
{
    if (s.size() == 0) return os << "{}";

    os << "{";
    bool first = true;
    for (const auto& nvp : s.fields) {
        if (!first)
            os << ", ";
        else
            first = false;
        os << "'" << nvp.first << "' : " << nvp.second;
    }
    os << "}";
    return os;
}
std::ostream& operator<<(std::ostream& os, const marray& m)
{
    if (m.is_empty()) return os << "[]";
    return m.impl->stream(os);
}
std::string marray::pt() const
{
    if (is_empty()) return "[]";
    return impl->pt();
}

namespace {
using void_deleter_t = std::function<void(void*)>;
using unique_void_ptr = std::unique_ptr<void, void_deleter_t>;

template <typename T>
auto void_deleter(void const* data) -> void
{
    auto p = static_cast<std::vector<T> const*>(data);
    delete p;
}

template <typename T>
auto unique_void(std::vector<T>* ptr) -> unique_void_ptr
{
    return unique_void_ptr(ptr, &void_deleter<T>);
}
}  // anonymous namespace

template <typename Scalar>
mnumeric_array::mnumeric_array(ndim dims, std::vector<Scalar> data)
    : ndim(std::move(dims)),
      data(unique_void<Scalar>(new std::vector<Scalar>(std::move(data)))),
      data_class(CTraits<Scalar>::mat_class())
{
}

#define FUNC_FOR_TYPES(F)    \
    F(double);               \
    F(std::complex<double>); \
    F(float);                \
    F(std::complex<float>);  \
    F(int64_t);              \
    F(uint64_t);             \
    F(int32_t);              \
    F(uint32_t);             \
    F(int16_t);              \
    F(uint16_t);             \
    F(int8_t);               \
    F(uint8_t);              \
    F(char);

#define MNUMERIC_ARRAY(T) \
    template mnumeric_array::mnumeric_array(ndim dims, std::vector<T> indata)
FUNC_FOR_TYPES(MNUMERIC_ARRAY)
#undef MNUMERIC_ARRAY

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& t)
{
    if (t.empty()) return os;
    os << "<" << typeid(T).name() << ">";
    os << t.at(0);
    for (std::size_t i = 1; i < t.size(); i++) os << ", " << t.at(i);
    return os;
}

// specialization for "strings", (u)int8 is the "numeric" type
template <>
std::ostream& operator<<(std::ostream& os, const std::vector<char>& t)
{
    os << "'" <<std::string(t.begin(), t.end()) << "'";
    return os;
}

template <typename Scalar>
const std::vector<Scalar>& mnumeric_array::as_type() const
{
    if (CTraits<Scalar>::mat_class() != data_class)
        throw std::bad_cast();  //"Cannot cast " + mio::to_string(type()) + " to
                                //" +
                                // mio::to_string(CTraits<Scalar>::mtype()));
    auto& internal = *static_cast<const std::vector<Scalar>*>(data.get());
    return internal;
}

std::ostream& operator<<(std::ostream& os, const mnumeric_array& n)
{
    std::fill_n(std::ostream_iterator<char>(os), n.dims().size(), '[');
    switch (n.data_class) {
        case MAT_CLASS::MAT_DOUBLE_CLASS:
            os << n.as_type<double>();
            break;
        case MAT_CLASS::MAT_FLOAT_CLASS:
            os << n.as_type<float>();
            break;
        case MAT_CLASS::MAT_INT64_CLASS:
            os << n.as_type<int64_t>();
            break;
        case MAT_CLASS::MAT_UINT64_CLASS:
            os << n.as_type<uint64_t>();
            break;
        case MAT_CLASS::MAT_INT32_CLASS:
            os << n.as_type<int32_t>();
            break;
        case MAT_CLASS::MAT_UINT32_CLASS:
            os << n.as_type<uint32_t>();
            break;
        case MAT_CLASS::MAT_INT16_CLASS:
            os << n.as_type<int16_t>();
            break;
        case MAT_CLASS::MAT_UINT16_CLASS:
            os << n.as_type<uint16_t>();
            break;
        case MAT_CLASS::MAT_INT8_CLASS:
            os << n.as_type<int8_t>();
            break;
        case MAT_CLASS::MAT_UINT8_CLASS:
            os << n.as_type<uint8_t>();
            break;
        case MAT_CLASS::MAT_CHAR_CLASS:
            os << n.as_type<char>();
            break;
        default:
            throw std::runtime_error("cannot stringify enum " +
                                     mio::to_string(n.data_class));
    }

    std::fill_n(std::ostream_iterator<char>(os), n.dims().size(), ']');
    return os;
}
std::string mnumeric_array::pt() const
{
    if (dims().empty()) return "0 x 1 []";
    return to_string(mat_class()) + " " + ndim::pt();
}

std::string mstruct::pt() const
{
    if (fields.empty()) return "{}";
    std::ostringstream os;
    os << "{";
    bool first = true;
    for (const auto& nvp : fields) {
        if (!first)
            os << ", ";
        else
            first = false;

        os << "'" << nvp.first << "' : " << nvp.second.pt();
    }
    os << "}";
    return os.str();
}

std::string mstruct_array::pt() const
{
    if (dims().empty()) return "0 x 1 [{}]";
    std::ostringstream os;
    os << ndim::pt() << " [";
    os << at(0).pt();
    for (std::size_t i = 1; i < size(); i++) {
        os << ", " << at(i).pt();
    }
    os << "]";
    return os.str();
}
std::string mcell_array::pt() const
{
    if (dims().empty()) return "0 x 1 [{}]";
    std::ostringstream os;
    os << ndim::pt() << " [";
    os << at(0).pt();
    for (std::size_t i = 1; i < size(); i++) {
        os << ", " << at(i).pt();
    }
    os << "]";
    return os.str();
}

std::ostream& operator<<(std::ostream& os, const mstruct_array& sa)
{
    if (sa.size() == 0) return os << "[]";

    os << "[";
    bool first = true;
    for (const mstruct& s : sa) {
        if (!first)
            os << ", ";
        else
            first = false;
        os << s;
    }
    return os << "]";
}

std::ostream& operator<<(std::ostream& os, const mcell_array& ca)
{
    if (ca.size() == 0) return os << "{}";

    os << "{";
    bool first = true;
    for (const marray& m : ca) {
        if (!first)
            os << ", ";
        else
            first = false;
        os << m;
    }
    return os << "}";
}

mstruct_array::mstruct_array(ndim dims, std::vector<mstruct> data)
    : nd_array(std::move(dims), std::move(data))
{
}

mstruct_array::mstruct_array(ndim dims) : nd_array(std::move(dims)) {}

mstruct_array::mstruct_array(mstruct&& data) : nd_array({1u, 1u}, {})
{
    this->at(0) = std::move(data);
}

mcell_array::mcell_array(ndim dims, std::vector<marray> data)
    : nd_array(std::move(dims), std::move(data))
{
}

mcell_array::mcell_array(ndim dims) : nd_array(std::move(dims)) {}

void mstruct::read(std::istream& is) { *this = mat5::read(is); }

void mstruct::write(std::ostream& os) { mat5::write(*this, os); }

}  // namespace mio
