#pragma once  // -*- c++ -*-

#if MIO_HAS_EIGEN
#include <Eigen/Core>
#endif
#include <functional>
#include <map>
#include <memory>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

#include "mtypes.h"
#include "ndim.h"

namespace mio {

class marray;

class mstruct {
  public:
    marray& field(const std::string& fn);
    const marray& field(const std::string& fn) const;
    bool has_field(const std::string& fn) const;
    std::size_t size() const { return fields.size(); }

    mstruct() = default;
    mstruct(std::string fn, marray&& data);
    mstruct(std::pair<std::string, marray>&& nvp);
    void set(std::string fn, marray&& data);
    void merge(mstruct&& other);
    marray drop(const std::string& fn);
    MAT_CLASS mat_class() const { return MAT_CLASS::MAT_STRUCT_CLASS; }
    mstruct(mstruct&& m) = default;
    mstruct& operator=(mstruct&& m) = default;
    friend std::ostream& operator<<(std::ostream& os, const mstruct& s);
    std::string pt() const;

    using iterator = std::map<std::string, marray>::iterator;
    using const_iterator = std::map<std::string, marray>::const_iterator;

    iterator begin() { return fields.begin(); }
    iterator end() { return fields.end(); }
    const_iterator begin() const { return fields.cbegin(); }
    const_iterator end() const { return fields.cend(); }

    // read mat5
    void read(std::istream& is);
    // write mat5
    void write(std::ostream& os);

  private:
    mstruct& operator=(const mstruct&) = delete;
    mstruct(const mstruct&) = delete;

    std::map<std::string, marray> fields;
};

class mnumeric_array : public ndim {
  public:
    MAT_CLASS mat_class() const { return data_class; }

    // Create a row vector from a list of Scalar data
    template <typename Scalar>
    static mnumeric_array row(std::initializer_list<Scalar> data)
    {
        return mnumeric_array({1, data.size()}, std::vector<Scalar>(data));
    }

    // Create a column vector from list of Scalar data
    template <typename Scalar>
    static mnumeric_array col(std::initializer_list<Scalar> data)
    {
        return mnumeric_array({data.size(), 1}, std::vector<Scalar>(data));
    }

    // Construct an MxN matrix from explicit dimensions and scalar input
    // Data is assumed to be in column major format and no re-shaping is done
    //
    template <typename Scalar>
    mnumeric_array(ndim dims, std::vector<Scalar> data);
    template <typename Scalar>
    mnumeric_array(ndim dims, std::initializer_list<Scalar> data)
        : mnumeric_array(std::move(dims), std::vector<Scalar>(data))
    {
    }

    template <typename Scalar>
    const std::vector<Scalar>& as_type() const;

    friend std::ostream& operator<<(std::ostream& os, const mnumeric_array& n);
    std::string pt() const;

    mnumeric_array(mnumeric_array&&) = default;
    mnumeric_array& operator=(mnumeric_array&&) = default;

  private:
    mnumeric_array(const mnumeric_array&) = delete;
    mnumeric_array& operator=(const mnumeric_array&) = delete;

    using void_deleter_t = std::function<void(void*)>;
    using unique_void_ptr = std::unique_ptr<void, void_deleter_t>;

    unique_void_ptr data;
    MAT_CLASS data_class;
};

class mstruct_array : public nd_array<mstruct> {
  public:
    // Allocate but don't initialize
    mstruct_array(ndim dims);
    // Value initialize, size of data is required to match size of dims
    mstruct_array(ndim dims, std::vector<mstruct> data);
    mstruct_array(mstruct&& data);
    MAT_CLASS mat_class() const { return MAT_CLASS::MAT_STRUCT_CLASS; }
    mstruct_array(mstruct_array&& m) = default;
    mstruct_array& operator=(mstruct_array&& m) = default;

    friend std::ostream& operator<<(std::ostream& os, const mstruct_array& sa);
    std::string pt() const;
};

class mcell_array : public nd_array<marray> {
  public:
    // Allocate but don't initialize
    mcell_array(ndim dims);
    // Value initialize, size of data is required to match size of dims
    mcell_array(ndim dims, std::vector<marray> data);
    mcell_array(mcell_array&& m) = default;
    MAT_CLASS mat_class() const { return MAT_CLASS::MAT_STRUCT_CLASS; }
    friend std::ostream& operator<<(std::ostream& os, const mcell_array& ca);
    std::string pt() const;
};

class marray_impl_base;
class marray : public ndim {
  public:
    bool is_numeric_array() const;
    bool is_struct_array() const;
    bool is_cell_array() const;
    bool is_empty() const;

    mnumeric_array& as_numeric_array();
    mcell_array& as_cell_array();
    mstruct_array& as_struct_array();
    const mnumeric_array& as_numeric_array() const;
    const mcell_array& as_cell_array() const;
    const mstruct_array& as_struct_array() const;

    marray();  // empty type, no conversion possible
    marray(marray&& other);
    marray& operator=(marray&& other);

    // create from numeric data
    marray(mnumeric_array data);
    // explicitly create 1D arrays of type
    template <typename Scalar>
    marray(std::initializer_list<Scalar> ns) : marray(mnumeric_array(ns))
    {
    }

    // create from struct array
    marray(mstruct_array data);

    // create from cell array
    marray(mcell_array data);

    void set(std::string fn, marray&& v);
    ~marray();

    // printing textual
    friend std::ostream& operator<<(std::ostream& os, const marray& m);
    // print type
    std::string pt() const;

  private:
    marray(const marray&) = delete;
    std::unique_ptr<marray_impl_base> impl;
};
}  // namespace mio
