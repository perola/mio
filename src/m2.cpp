#include <fstream>
#include <iostream>
#if MIO_HAS_EIGEN
#include <Eigen/Core>
#endif

#include "mio.h"
using namespace mio;

int main(int argc, char* argv[])
{
    marray m;
    mnumeric_array mn =
        mnumeric_array::col({1.0, 2.0, 3.14});  // from initializer list
    std::cerr << "mn: " << mn << std::endl;
    mnumeric_array mdim = mnumeric_array(ndim(2), {1.0, 2.0});
    std::cerr << "mdim: " << mdim << std::endl;

    mstruct_array ma({1, 2});
    std::cerr << ndim(0).pt() << ", " << ndim(2, 2).pt() << ", "
              << ndim(1.0, 2, 3, 4, 65).pt();
    std::cerr << "ma: " << ma << std::endl;
    ma.at(1) = {"s", mnumeric_array::row({1.1, -1.1})};
    ma.at(0) = {"s", mcell_array({2, 2})};  //{"s", {2.1, -2.1}};
    mstruct ms("s", marray(mnumeric_array::col({1.0, 2.0, 3.145})));
    auto ms2 = std::move(ms);

    std::cerr << "li: "
              << ndim::linear_index(std::vector<std::size_t>{3, 4}, {1, 2})
              << std::endl;
    std::cerr << "li: " << ndim::linear_index(std::vector<std::size_t>{3, 2, 2},
                                              {2, 1, 1})
              << std::endl;
    std::cerr << "ma: " << ma << R"(.  .at(1).at("s").is_numeric? )"
              << ma.at(1).field("s").is_numeric_array() << std::endl;
    std::cerr << "ms: " << ms2.field("s").is_numeric_array() << std::endl;

    marray m2 = marray(std::move(ma));
    std::cerr << "m2: " << m2 << std::endl;

    std::cerr << mcell_array({1, 3}) << std::endl;

    const auto& mr = m2.as_struct_array();
    std::cerr << "mr: " << mr << std::endl;
    // const auto & mwr = m2.as_cell_array();
    // std::cerr << "mwr: " << mwr << std::endl;

    if (argc < 1) return 0;

    std::ifstream in(argv[1]);
    if (!in) return 1;
    mstruct read;
    read.read(in);
    std::cerr << "read: " << read.pt() << std::endl;
    std::cerr << "read: " << read << std::endl;

    return 0;
}
