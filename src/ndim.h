#pragma once  // -*- c++ -*-

#include <algorithm>
#include <sstream>
#include <vector>

#include "mtraits.h"

namespace mio {

class ndim {
  public:
    std::size_t rows() const { return dim.at(0); }
    std::size_t cols() const { return dim.at(1); }
    const std::vector<std::size_t>& dims() const { return dim; }
    std::size_t size() const
    {
        return std::accumulate(dim.begin(), dim.end(), 1,
                               std::multiplies<std::size_t>());
    }
    ndim(ndim&&) = default;
    ndim& operator=(ndim&&) = default;
    ndim(std::vector<std::size_t> dims = {}) : dim(std::move(dims)) {}

    static std::size_t linear_index(
        const std::vector<std::size_t>& dims,
        const std::initializer_list<std::size_t>& coord)
    {
        /*
            \sum_{i=0}^{d-1} \left ( \prod_{j=0}^{i-1}N_j\right ) n_i
            = n_1 + N_1 \cdot (n_2+N_2\cdot(n_3+N_3 \cdot(\dots )))

            Ex: arr: 3 x 4, coord (1,2) -> 1 + 3*2 = 7
            0 3 6  9
            1 4 7 10
            2 5 8 11

            Ex: arr 3 x 2 x 2, coord (2,1,1) -> 2 + 3*(1 + 2*(1)) = 11
            0 3  6  9
            1 4  7 10
            2 5  8 11
           */
        std::size_t li = 0;
        std::size_t mult = 1;

        // the sum iteration, skip first: already initialized
        for (std::size_t i = 0; i < coord.size(); i++) {
            li += mult * *(coord.begin() + i);
            mult *= dims.at(i);
        }
        return li;
    }
    template <typename... Inds>
    ndim(std::size_t i0, Inds... ii)
        : ndim(std::initializer_list<std::size_t>{
              i0, static_cast<std::size_t>(ii)...})
    {
    }
    std::string pt() const
    {
        std::ostringstream os;
        os << dims().at(0);
        for (std::size_t i = 1; i < dims().size(); i++)
            os << " x " << dims().at(i);
        return os.str();
    }

  private:
    std::vector<std::size_t> dim = {0, 1};
};

template <typename T>
class nd_array : public ndim {
  public:
    template <typename... Inds>
    T& at(Inds... ii)
    {
        return data.at(
            linear_index(dims(), std::initializer_list<std::size_t>{
                                     static_cast<std::size_t>(ii)...}));
    }
    template <typename... Inds>
    const T& at(Inds... ii) const
    {
        return data.at(
            linear_index(dims(), std::initializer_list<std::size_t>{
                                     static_cast<std::size_t>(ii)...}));
    }
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;
    iterator begin() { return data.begin(); }
    iterator end() { return data.end(); }
    const_iterator begin() const { return data.begin(); }
    const_iterator end() const { return data.end(); }

    nd_array(ndim indims) : ndim(std::move(indims)), data(size()) {}

    nd_array(ndim indims, std::vector<T> indata)
        : ndim(std::move(indims)), data(std::move(indata))
    {
        if (data.size() != size())
            throw std::runtime_error("size mismatch in nd_array constructor");
        assert(data.size() == size());
    }
    nd_array(nd_array&&) = default;
    nd_array& operator=(nd_array&&) = default;

  private:
    std::vector<T> data;
};

}  // namespace mio
