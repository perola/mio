#include "mat5.h"

#include "mio.h"
#include "mtraits.h"
#include "mtypes.h"
#include "oneshotreadbuf.h"

#define ZLIB_CONST
#include <zlib.h>
#include <algorithm>
#include <array>
#include <fstream>
#include <functional>
#include <iostream>
#include <istream>
#include <numeric>
#include <ostream>
#include <stdexcept>
#include <vector>

#include <codecvt>
#include <locale>

// Runtime assert throws
#define RT_ASSERT(expr, errstring) \
    if (!(expr)) throw std::runtime_error(errstring);

namespace mio {
namespace mat5 {

// Try to read into a scalar type
template <typename T,
          typename std::enable_if<std::is_scalar<T>::value, int>::type = 0>
auto try_read(std::istream& is, T& t) -> decltype(sizeof(0))
{
    is.read(reinterpret_cast<char*>(&t), sizeof(T));
    RT_ASSERT(!!is, "try_read<Scalar> error");
    return sizeof(T);
}

/* Try to read into an array like compounds
   Requires size(), data() and operator[] members
 */
template <typename Array>
auto try_read(std::istream& is, Array& a)
    -> decltype(a[0], a.size(), a.data(), sizeof(0))
{
    auto numChars = sizeof(decltype(a[0])) * a.size();
    is.read(reinterpret_cast<char*>(a.data()), numChars);
    RT_ASSERT(!!is, "try_read<Array_t> error");
    return numChars;
}

// Try to read into an array like compound, specialization for std::string
template <>
auto try_read<std::string>(std::istream& is, std::string& a)
    -> decltype(a.size())
{
    auto numChars = sizeof(decltype(a[0])) * a.size();
    is.read(reinterpret_cast<char*>(&a.at(0)), numChars);
    RT_ASSERT(!!is, "try_read<std::string> error");
    return numChars;
}

/** Try to read into a static array
 */
template <class T, std::size_t N>
bool try_read(std::istream& is, T (&t)[N])
{
    auto numChars = sizeof(T) * N;
    is.read(reinterpret_cast<char*>(t), numChars);
    RT_ASSERT(!!is, "try_read<T(&t)[N]> error");
    return numChars;
}

/** Try to read a typed vector type (including header) from stream
    vector = [section_header data]
    The data is not necessarilly encoded with the same data type as
    the vector itself, thus the need for extra header.
  */
template <class T>
std::vector<T> try_read(std::istream& is);

/** The mat5 reader entry point, reads a file header end ensures
    correct format and endianness.
    Then proceeds to extract name-value-pairs from the stream
 */
mstruct read(std::istream& is)
{
    file_header header;
    header.read(is);

    RT_ASSERT(header.endian == (std::array<char, 2>({{'I', 'M'}})),
              "endian swapping not implemented");
    RT_ASSERT(header.version == matlab::version_5, "Only supports mat v5");

    mstruct all;
    while (is.peek() != std::istream::traits_type::eof()) {
        auto nvp = read_one(is);
        all.set(std::move(nvp.first), std::move(nvp.second));
    }
    return all;
}

void write(const mstruct& mt, std::ostream& os)
{
    file_header h;
    h.write(os);
    for (const auto& nvp : mt) {
        write_one(nvp, os);
    }
}

std::ostream& operator<<(std::ostream& os, const section_header& h)
{
    os << "{ \n";
    os << "   t: " << h.data_type << "\n"
       << "  #b: " << h.num_dbytes << "\n"
       << "   w: " << h.num_wbytes << "\n"
       << (h.small ? "    : small\n" : "") << " pad: " << h.padding() << "\n";
    os << "}";
    return os;
}

template <typename T>
void try_write(const T& t, std::ostream& os)
{
    os.write(reinterpret_cast<const char*>(&t), sizeof(t));
}

void section_header::write(std::ostream& os) const
{
    try_write(data_type, os);
    try_write(num_dbytes, os);
}

uint16_t high(uint32_t combined) { return combined >> 16; }

uint16_t low(uint32_t combined)
{
    uint32_t mask = std::numeric_limits<uint16_t>::max();
    return mask & combined;
}

void section_header::read(std::istream& is)
{
    small = false;
    auto numRead = try_read(is, data_type);

    auto dt = static_cast<uint32_t>(data_type);
    if (high(dt) != 0) {
        // the small data format packs both type and size into the same uint32_t
        num_dbytes = high(dt);
        data_type = static_cast<MAT_TYPE>(low(dt));
        small = true;
    } else {
        numRead += try_read(is, num_dbytes);
    }

    if (small) {
        num_wbytes = 8;  // don't understand this crap
        assert(numRead == 4);
    } else if (data_type == MAT_TYPE::MAT_COMPRESSED)
        num_wbytes = 8 + num_dbytes;
    else
        num_wbytes = 8 + num_dbytes + ((8 - num_dbytes) % 8);
}

std::vector<char> inflate_blob(section_header& header,
                               const std::vector<char>& blob)
{
    z_stream zstream{};  // zero initialize
    RT_ASSERT(inflateInit(&zstream) == Z_OK, "decompr header failure");

    // first extract the the section header, ie 8 bytes from the blob
    //
    zstream.avail_in = blob.size();
    zstream.next_in = reinterpret_cast<const unsigned char*>(blob.data());
    {
        char buf[8];
        zstream.avail_out = sizeof(buf) / sizeof(*buf);
        zstream.next_out = reinterpret_cast<unsigned char*>(buf);
        // decompress header
        RT_ASSERT(inflate(&zstream, Z_NO_FLUSH) == Z_OK,
                  "decompr header failure");

        // read the header from the inflated buffer
        OneShotReadBuf rb(buf, sizeof(buf) / sizeof(*buf));
        std::istream is(&rb);
        header.read(is);
        RT_ASSERT(!!is, "decompr header failure");
    }

    // allocate output with known size
    std::vector<char> ublob(header.num_dbytes);
    // set output to new blob
    zstream.avail_out = ublob.size();
    zstream.next_out = reinterpret_cast<unsigned char*>(ublob.data());

    // try to decompress all remaining data
    RT_ASSERT(inflate(&zstream, Z_FINISH) == Z_STREAM_END,
              "decompr header failure");
    // deinit
    RT_ASSERT(inflateEnd(&zstream) == Z_OK, "decompr header failure");

    return ublob;
}
std::ostream& operator<<(std::ostream& os, const var_header& h)
{
    os << "{\n";
    os << "  '" << h.name << "'\n";
    os << "  " << h.enc_data_type << "\n";
    os << "  " << h.dims.at(0);
    for (std::size_t i = 1; i < h.dims.size(); i++) os << " x " << h.dims.at(i);
    os << "\n";
    os << "}";
    return os;
}

std::ostream& operator<<(std::ostream& os, const std::vector<char>& str)
{
    os << "{ ";
    bool first = true;
    for (char c : str) {
        if (!first)
            os << ", ";
        else
            first = false;
        os << "'" << c << "'" << int(c);
    }
    return os << " }";
}

std::ostream& operator<<(std::ostream& os, const std::vector<uint32_t>& str)
{
    os << "{ ";
    bool first = true;
    for (auto c : str) {
        if (!first)
            os << ", ";
        else
            first = false;
        os << c;
    }
    return os << " }";
}

template <typename T>
bool flag(const T& t, int bit)
{
    return t & (T{1} << bit);
}

void var_header::read(std::istream& is)
{
    char buffer[16];
    /*auto numRead =*/ try_read(is, buffer);

    complex = flag(buffer[9], 3);
    enc_data_type = static_cast<MAT_CLASS>(buffer[8]);

    // read a compound: section_header + data
    dims = try_read<uint32_t>(is);

    switch (dims.size()) {
        default:
        //            std::cerr << "dimensionality > 2, got "
        //            << std::to_string(dims.size()) << std::endl;
        case 2:
            break;
        case 1:
            break;
        case 0:
            std::cerr << "warning: zero dim encountered" << std::endl;
    }
    // read a compound string: section_header + data
    auto namevec = try_read<char>(is);
    auto it = std::find(namevec.begin(), namevec.end(), '\0');
    name = std::string(namevec.begin(), it);
}

mcell_array read_cell_array(std::istream& is, const std::vector<uint32_t>& dims)
{
    std::vector<std::size_t> sdim(dims.size());
    std::copy(dims.begin(), dims.end(), sdim.begin());
    mcell_array ret(sdim);
    for (std::size_t i = 0; i < ret.size(); i++) {
        ret.at(i) = read_one(is).second;
    }
    return ret;
}

mstruct_array read_struct_array(std::istream& is,
                                const std::vector<uint32_t>& dims)
{
    /* struct layout
      [length_header, length@int32_t, fields_header,  real_data, imag_data]

     */
    std::vector<int32_t> length = try_read<int32_t>(is);
    assert(length.size() == 1);
    std::vector<char> names = try_read<char>(is);

    unsigned int noFields = names.size() / length.front();
    mstruct_array ret(std::vector<std::size_t>(dims.begin(), dims.end()));
    for (unsigned int i = 0; i < ret.size(); i++) {
        mstruct str;
        for (unsigned int j = 0; j < noFields; j++) {
            auto it = std::find(names.begin() + j * length.front(), names.end(),
                                '\0');
            std::string name =
                std::string(names.begin() + j * length.front(), it);

            str.set(name, read_one(is).second);
        }
        ret.at(i) = std::move(str);
    }
    return ret;
}

template <typename T>
typename std::enable_if<valid_complex<T>::value, mnumeric_array>::type
read_complex(std::istream& is, const var_header& h)
{
    std::vector<T> real = try_read<T>(is);
    std::vector<T> complex = try_read<T>(is);
    std::vector<std::complex<T>> combined(real.size());
    std::transform(
        real.begin(), real.end(), complex.begin(), combined.begin(),
        [](const T& r, const T& c) { return std::complex<T>(r, c); });
    return mnumeric_array(
        std::vector<std::size_t>(h.dims.begin(), h.dims.end()),
        std::move(combined));
}

template <typename T>
typename std::enable_if<!valid_complex<T>::value, mnumeric_array>::type
read_complex(std::istream& /*is*/, const var_header& /*h*/)
{
    assert(false && "unreachable");
}

template <typename T>
mnumeric_array read_numeric(std::istream& is, const var_header& h)
{
    if (h.complex) return read_complex<T>(is, h);

    return mnumeric_array(
        std::vector<std::size_t>(h.dims.begin(), h.dims.end()),
        try_read<T>(is));
}

std::pair<std::string, marray> read_var(std::istream& is)
{
    /* var layout
      [var_header,  real_data, imag_data]

     */
    var_header var_h;
    var_h.read(is);
    if (!is) throw std::runtime_error("read_var header failure");

    marray ret;
    switch (var_h.enc_data_type) {
        case MAT_CLASS::MAT_DOUBLE_CLASS:
            return {var_h.name, read_numeric<double>(is, var_h)};
        case MAT_CLASS::MAT_FLOAT_CLASS:
            return {var_h.name, read_numeric<float>(is, var_h)};
        case MAT_CLASS::MAT_INT8_CLASS:
            return {var_h.name, read_numeric<int8_t>(is, var_h)};
        case MAT_CLASS::MAT_UINT8_CLASS:
            return {var_h.name, read_numeric<uint8_t>(is, var_h)};
        case MAT_CLASS::MAT_INT16_CLASS:
            return {var_h.name, read_numeric<int16_t>(is, var_h)};
        case MAT_CLASS::MAT_UINT16_CLASS:
            return {var_h.name, read_numeric<uint16_t>(is, var_h)};
        case MAT_CLASS::MAT_INT32_CLASS:
            return {var_h.name, read_numeric<int32_t>(is, var_h)};
        case MAT_CLASS::MAT_UINT32_CLASS:
            return {var_h.name, read_numeric<uint32_t>(is, var_h)};
        case MAT_CLASS::MAT_INT64_CLASS:
            return {var_h.name, read_numeric<int64_t>(is, var_h)};
        case MAT_CLASS::MAT_UINT64_CLASS:
            return {var_h.name, read_numeric<uint64_t>(is, var_h)};
        case MAT_CLASS::MAT_CELL_CLASS:
            return {var_h.name, read_cell_array(is, var_h.dims)};
        case MAT_CLASS::MAT_STRUCT_CLASS:
            return {var_h.name, read_struct_array(is, var_h.dims)};
        case MAT_CLASS::MAT_CHAR_CLASS:
            return {var_h.name, read_numeric<char>(is, var_h)};
        case MAT_CLASS::MAT_SPARSE_CLASS:
            throw std::runtime_error("sparse not supported");
        default: {
            throw std::runtime_error("unknown matlab class: " +
                                     to_string(var_h.enc_data_type));
        }
    }

    return {"", marray()};
}

void write_one(const std::pair<const std::string, marray>& nvp,
               std::ostream& os)
{
    section_header varh = section_header::from(nvp.second);
    varh.write(os);

    assert(varh.data_type == MAT_TYPE::MAT_MATRIX);
}

std::pair<std::string, marray> read_one(std::istream& is)
{
    section_header varh{};
    varh.read(is);
    RT_ASSERT(!!is, "section header read failure");

    // read the binary data block
    std::vector<char> data(varh.num_dbytes);
    RT_ASSERT(try_read(is, data) == varh.num_dbytes, "bad mat file");

    switch (varh.data_type) {
        case MAT_TYPE::MAT_COMPRESSED:
            data = inflate_blob(varh, data);
            break;
        case MAT_TYPE::MAT_MATRIX: {
            std::streampos head_pos = is.tellg();
            int padding = head_pos % 8;
            is.seekg(padding, std::fstream::cur);
        } break;
        default:
            throw std::runtime_error("unsupported mat type: " +
                                     to_string(varh.data_type));
    }

    OneShotReadBuf buf(data);
    std::istream blob(&buf);

    return read_var(blob);
}

template <size_t N>
std::ostream& operator<<(std::ostream& os, const std::array<char, N>& str)
{
    return os.write(str.data(), N);
}

void file_header::write(std::ostream& os) const
{
    os.write(header.data(), header.size());
    os.write(subsys.data(), subsys.size());
    os.write(reinterpret_cast<const char*>(&version), sizeof(version));
    os.write(endian.data(), endian.size());
}
void file_header::read(std::istream& is)
{
    // get the header information from the Mat file
    try_read(is, header);
    try_read(is, subsys);
    try_read(is, version);
    try_read(is, endian);

    auto head_pos = is.tellg();
    assert(head_pos == 128);
}

template <typename Tout, typename Tenc>
uint32_t try_read_convert(std::istream& is, std::vector<Tout>& output)
{
    if (std::is_same<Tout, Tenc>::value) {
        return try_read(is, output);
    }

    // we know the number of values to read
    std::vector<Tenc> encoded(output.size());
    auto numRead = try_read(is, encoded);
    std::transform(encoded.begin(), encoded.end(), output.begin(),
                   [](const Tenc& t) -> Tout { return t; });
    return numRead;
}

template <>
uint32_t try_read_convert<char, char16_t>(std::istream& is,
                                          std::vector<char>& output)
{
    // we know the number of values to read
    std::vector<char16_t> encoded(output.size());
    auto numRead = try_read(is, encoded);

    std::wstring_convert<
        std::codecvt_utf8_utf16<char16_t, 0x10ffff,
                                std::codecvt_mode::little_endian>,
        char16_t>
        cnv;
    std::string utf8 =
        cnv.to_bytes(std::u16string(encoded.begin(), encoded.end()));

    std::cerr << "UTF8 " << utf8.size() << ": " << utf8 << std::endl;
    output = std::vector<char>(utf8.begin(), utf8.end());

    return numRead;
}

template <class T>
std::vector<T> try_read(std::istream& is)
{
    section_header h{};
    h.read(is);
    RT_ASSERT(!!is, "section header read failure");

    std::vector<T> t;
    uint32_t numRead;

    switch (h.data_type) {
        case MAT_TYPE::MAT_INT8:
            t.resize(h.num_dbytes / sizeof(int8_t));
            numRead = try_read_convert<T, int8_t>(is, t);
            break;
        case MAT_TYPE::MAT_UINT8:
            t.resize(h.num_dbytes / sizeof(uint8_t));
            numRead = try_read_convert<T, uint8_t>(is, t);
            break;
        case MAT_TYPE::MAT_INT16:
            t.resize(h.num_dbytes / sizeof(int16_t));
            numRead = try_read_convert<T, int16_t>(is, t);
            break;
        case MAT_TYPE::MAT_UINT16:
            t.resize(h.num_dbytes / sizeof(uint16_t));
            numRead = try_read_convert<T, uint16_t>(is, t);
            break;
        case MAT_TYPE::MAT_INT32:
            t.resize(h.num_dbytes / sizeof(int32_t));
            numRead = try_read_convert<T, int32_t>(is, t);
            break;
        case MAT_TYPE::MAT_UINT32:
            t.resize(h.num_dbytes / sizeof(uint32_t));
            numRead = try_read_convert<T, uint32_t>(is, t);
            break;
        case MAT_TYPE::MAT_FLOAT:
            t.resize(h.num_dbytes / sizeof(float));
            numRead = try_read_convert<T, float>(is, t);
            break;
        case MAT_TYPE::MAT_DOUBLE:
            t.resize(h.num_dbytes / sizeof(double));
            numRead = try_read_convert<T, double>(is, t);
            break;
        case MAT_TYPE::MAT_INT64:
            t.resize(h.num_dbytes / sizeof(int64_t));
            numRead = try_read_convert<T, int64_t>(is, t);
            break;
        case MAT_TYPE::MAT_UINT64:
            t.resize(h.num_dbytes / sizeof(uint64_t));
            numRead = try_read_convert<T, uint64_t>(is, t);
            break;
        case MAT_TYPE::MAT_UTF8:
            t.resize(h.num_dbytes / sizeof(char));
            numRead = try_read_convert<T, char>(is, t);
            break;
        case MAT_TYPE::MAT_MATRIX:
        case MAT_TYPE::MAT_COMPRESSED:
        case MAT_TYPE::MAT_UTF16:
            t.resize(h.num_dbytes / sizeof(char16_t));
            numRead = try_read_convert<T, char16_t>(is, t);
            break;
        case MAT_TYPE::MAT_UTF32:
        default:
            throw std::runtime_error("cannot handle type " +
                                     to_string(h.data_type) + " in conversion");
            break;
    }

    RT_ASSERT(numRead == h.num_dbytes, "numread mismatch");

    auto numPad = h.padding();
    if (numPad > 0) {
        RT_ASSERT(!!is.seekg(static_cast<int>(numPad), std::ios::cur),
                  "bad seek");
    }
    return t;
}
}  // namespace mat5
}  // namespace mio
