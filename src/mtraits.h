#pragma once
#include "mtypes.h"

namespace mio {

template <class T>
struct valid_complex : std::false_type
{
};
template <>
struct valid_complex<double> : std::true_type
{
};
// template<> struct valid_complex<long double> : std::true_type {};
template <>
struct valid_complex<float> : std::true_type
{
};

template <typename T>
struct CTraitsBase
{
    using value_type = T;
#if MIO_HAS_EIGEN
    using ArrayT = Eigen::Array<T, -1, -1>;
#endif
};

template <typename T>
struct CTraits;

template <>
struct CTraits<double> : public CTraitsBase<double>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_DOUBLE_CLASS;
    }
};
template <>
struct CTraits<std::complex<double>> : public CTraitsBase<std::complex<double>>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_DOUBLE_CLASS;
    }
};
template <>
struct CTraits<float> : public CTraitsBase<float>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_FLOAT_CLASS;
    }
};
template <>
struct CTraits<std::complex<float>> : public CTraitsBase<std::complex<float>>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_FLOAT_CLASS;
    }
};
template <>
struct CTraits<int8_t> : public CTraitsBase<int8_t>
{
    static constexpr MAT_CLASS mat_class() { return MAT_CLASS::MAT_INT8_CLASS; }
};
template <>
struct CTraits<uint8_t> : public CTraitsBase<uint8_t>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_UINT8_CLASS;
    }
};
template <>
struct CTraits<int16_t> : public CTraitsBase<int16_t>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_INT16_CLASS;
    }
};
template <>
struct CTraits<uint16_t> : public CTraitsBase<uint16_t>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_UINT16_CLASS;
    }
};
template <>
struct CTraits<int32_t> : public CTraitsBase<int32_t>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_INT32_CLASS;
    }
};
template <>
struct CTraits<uint32_t> : public CTraitsBase<uint32_t>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_UINT32_CLASS;
    }
};
template <>
struct CTraits<int64_t> : public CTraitsBase<int64_t>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_INT64_CLASS;
    }
};
template <>
struct CTraits<uint64_t> : public CTraitsBase<uint64_t>
{
    static constexpr MAT_CLASS mat_class()
    {
        return MAT_CLASS::MAT_UINT64_CLASS;
    }
};
template <>
struct CTraits<char> : public CTraitsBase<char>
{
    static constexpr MAT_CLASS mat_class() { return MAT_CLASS::MAT_CHAR_CLASS; }
};

// the "reversed" traits, from matlab type to c-type
template <MAT_CLASS mclass>
struct MTraits;
template <>
struct MTraits<MAT_CLASS::MAT_DOUBLE_CLASS> : CTraits<double>
{
};
template <>
struct MTraits<MAT_CLASS::MAT_FLOAT_CLASS> : CTraits<float>
{
};
template <>
struct MTraits<MAT_CLASS::MAT_INT8_CLASS> : CTraits<int8_t>
{
};
template <>
struct MTraits<MAT_CLASS::MAT_UINT8_CLASS> : CTraits<uint8_t>
{
};
template <>
struct MTraits<MAT_CLASS::MAT_INT16_CLASS> : CTraits<int16_t>
{
};
template <>
struct MTraits<MAT_CLASS::MAT_UINT16_CLASS> : CTraits<uint16_t>
{
};
template <>
struct MTraits<MAT_CLASS::MAT_INT32_CLASS> : CTraits<int32_t>
{
};
template <>
struct MTraits<MAT_CLASS::MAT_UINT32_CLASS> : CTraits<uint32_t>
{
};
template <>
struct MTraits<MAT_CLASS::MAT_INT64_CLASS> : CTraits<int64_t>
{
};
template <>
struct MTraits<MAT_CLASS::MAT_UINT64_CLASS> : CTraits<uint64_t>
{
};
}
