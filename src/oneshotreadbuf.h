#pragma once

#include <streambuf>
#include <vector>

namespace mio {
// https://stackoverflow.com/q/1448467
// https://stackoverflow.com/a/46069245
struct OneShotReadBuf : public std::streambuf
{
    OneShotReadBuf(char* s, std::size_t n) { setg(s, s, s + n); }
    OneShotReadBuf(const std::vector<char>& data, int offset = 0)
        : OneShotReadBuf(const_cast<char*>(data.data()) + offset,
                         data.size() - offset)
    {
    }
    pos_type seekpos(pos_type sp, std::ios_base::openmode which) override
    {
        return seekoff(sp - pos_type(off_type(0)), std::ios_base::beg, which);
    }
    pos_type seekoff(
        off_type off, std::ios_base::seekdir dir,
        std::ios_base::openmode /*which = std::ios_base::in*/) override
    {
        if (dir == std::ios_base::cur)
            gbump(off);
        else if (dir == std::ios_base::end)
            setg(eback(), egptr() + off, egptr());
        else if (dir == std::ios_base::beg)
            setg(eback(), eback() + off, egptr());
        return gptr() - eback();
    }
};

}  // namespace mio
