#pragma once

#include "mio.h"

#include <array>
#include <iostream>
#include <istream>
#include <ostream>
#include <vector>

namespace mio {

namespace mat5 {

enum class matlab : int16_t
{
    version_unknown = 0x0,
    version_5 = 0x0100,
    version_73 = 0x0200
};

/* Mat file layout
     mat = [mat_header nvp*]
     nvp = [section_header blob]
     blob = compressed_blob | [var padding]
     compressed_blob = [var padding]
     var = [var_header [section_header realdata] [section_header imagdata]]

     mat_header = [header[116], subsys[8], version[2], endian[2]]
     header: "MATLAB 5.0 MAT-file, Platform: MACI64, Created on: Sun Feb 11
   17:35:45 2018                                         "
     subsys: [0,0,0,0,0,0,0,0]
     version: 0x0100 | 0x0200
     endian: "IM" | "MI"

   */
mstruct read(std::istream& is);
void write(const mstruct& mt, std::ostream& os);

struct section_header
{
    MAT_TYPE data_type;   // the type of expanded var
    uint32_t num_dbytes;  // num bytes of payload
    uint32_t num_wbytes;  // total width of var
    bool small;
    // the number of bytes of padding after the data section
    uint32_t padding() const
    {
        return num_wbytes - num_dbytes - (small ? 4 : 8);
    }
    friend std::ostream& operator<<(std::ostream& os, const section_header& h);

    void read(std::istream& is);
    void write(std::ostream& os) const;
    static section_header from(const marray& /*arr*/)
    {
        section_header header{};
        header.data_type = MAT_TYPE::MAT_MATRIX;
        return header;
    }
};
std::vector<char> inflate_blob(section_header& header,
                               const std::vector<char>& blob);

struct var_header
{
    bool complex;
    MAT_CLASS enc_data_type;
    std::vector<uint32_t> dims = {0, 1};
    std::string name;

    friend std::ostream& operator<<(std::ostream& os, const var_header& h);

    void read(std::istream& is);
    void write(std::ostream& os) const;
};

struct file_header
{
    std::array<char, 116> header{};
    std::array<char, 8> subsys{};
    std::array<char, 2> endian{{'I', 'M'}};
    matlab version = matlab::version_5;

    void read(std::istream& is);
    void write(std::ostream& os) const;
};

// read any marray type
std::pair<std::string, marray> read_var(std::istream& is);

// read a struct_array
mstruct_array read_struct_array(std::istream& is,
                                const std::vector<uint32_t>& dims);

// read a cell array
mcell_array read_cell_array(std::istream& is,
                            const std::vector<uint32_t>& dims);

marray read_blob(const section_header& header, const std::vector<char>& blob);

// read an nvp
std::pair<std::string, marray> read_one(std::istream& is);

void write_one(const std::pair<const std::string, marray>& nvp,
               std::ostream& is);

matlab read_header(std::istream& is);

}  // namespace mat5
}  // namespace mio
