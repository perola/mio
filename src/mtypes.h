#pragma once

#include <cassert>
#include <iostream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <complex>

namespace mio {
enum class MAT_TYPE : uint32_t
{
    MAT_INT8 = 1,
    MAT_UINT8 = 2,
    MAT_INT16 = 3,
    MAT_UINT16 = 4,
    MAT_INT32 = 5,
    MAT_UINT32 = 6,
    MAT_FLOAT = 7,
    MAT_DOUBLE = 9,
    MAT_INT64 = 12,
    MAT_UINT64 = 13,
    MAT_MATRIX = 14,
    MAT_COMPRESSED = 15,
    MAT_UTF8 = 16,
    MAT_UTF16 = 17,
    MAT_UTF32 = 18
};

inline std::string to_string(const MAT_TYPE& m)
{
    switch (m) {
        case MAT_TYPE::MAT_INT8:
            return "MAT_INT8";
        case MAT_TYPE::MAT_UINT8:
            return "MAT_UINT8";
        case MAT_TYPE::MAT_INT16:
            return "MAT_INT16";
        case MAT_TYPE::MAT_UINT16:
            return "MAT_UINT16";
        case MAT_TYPE::MAT_INT32:
            return "MAT_INT32";
        case MAT_TYPE::MAT_UINT32:
            return "MAT_UINT32";
        case MAT_TYPE::MAT_FLOAT:
            return "MAT_FLOAT";
        case MAT_TYPE::MAT_DOUBLE:
            return "MAT_DOUBLE";
        case MAT_TYPE::MAT_INT64:
            return "MAT_INT64";
        case MAT_TYPE::MAT_UINT64:
            return "MAT_UINT64";
        case MAT_TYPE::MAT_MATRIX:
            return "MAT_MATRIX";
        case MAT_TYPE::MAT_COMPRESSED:
            return "MAT_COMPRESSED";
        case MAT_TYPE::MAT_UTF8:
            return "MAT_UTF8";
        case MAT_TYPE::MAT_UTF16:
            return "MAT_UTF16";
        case MAT_TYPE::MAT_UTF32:
            return "MAT_UTF32";

        default: {
            throw std::runtime_error(
                "bad MAT_TYPE: " +
                std::to_string(
                    static_cast<std::underlying_type<MAT_TYPE>::type>(m)));
        }
    }
    return "";
}

inline std::ostream& operator<<(std::ostream& os, const MAT_TYPE& m)
{
    return os << to_string(m);
}

enum class MAT_CLASS : char
{
    MAT_CELL_CLASS = 1,
    MAT_STRUCT_CLASS = 2,
    MAT_OBJECT_CLASS = 3,
    MAT_CHAR_CLASS = 4,
    MAT_SPARSE_CLASS = 5,
    MAT_DOUBLE_CLASS = 6,
    MAT_FLOAT_CLASS = 7,
    MAT_INT8_CLASS = 8,
    MAT_UINT8_CLASS = 9,
    MAT_INT16_CLASS = 10,
    MAT_UINT16_CLASS = 11,
    MAT_INT32_CLASS = 12,
    MAT_UINT32_CLASS = 13,
    MAT_INT64_CLASS = 14,
    MAT_UINT64_CLASS = 15
};

inline std::string to_string(const MAT_CLASS& m)
{
    switch (m) {
        case MAT_CLASS::MAT_CELL_CLASS:
            return "MAT_CELL_CLASS";
        case MAT_CLASS::MAT_STRUCT_CLASS:
            return "MAT_STRUCT_CLASS";
        case MAT_CLASS::MAT_OBJECT_CLASS:
            return "MAT_OBJECT_CLASS";
        case MAT_CLASS::MAT_CHAR_CLASS:
            return "MAT_CHAR_CLASS";
        case MAT_CLASS::MAT_SPARSE_CLASS:
            return "MAT_SPARSE_CLASS";
        case MAT_CLASS::MAT_DOUBLE_CLASS:
            return "MAT_DOUBLE_CLASS";
        case MAT_CLASS::MAT_FLOAT_CLASS:
            return "MAT_FLOAT_CLASS";
        case MAT_CLASS::MAT_INT8_CLASS:
            return "MAT_INT8_CLASS";
        case MAT_CLASS::MAT_UINT8_CLASS:
            return "MAT_UINT8_CLASS";
        case MAT_CLASS::MAT_INT16_CLASS:
            return "MAT_INT16_CLASS";
        case MAT_CLASS::MAT_UINT16_CLASS:
            return "MAT_UINT16_CLASS";
        case MAT_CLASS::MAT_INT32_CLASS:
            return "MAT_INT32_CLASS";
        case MAT_CLASS::MAT_UINT32_CLASS:
            return "MAT_UINT32_CLASS";
        case MAT_CLASS::MAT_INT64_CLASS:
            return "MAT_INT64_CLASS";
        case MAT_CLASS::MAT_UINT64_CLASS:
            return "MAT_UINT64_CLASS";
        default:
            assert(false);
            return "";
    }
}

inline std::ostream& operator<<(std::ostream& os, const MAT_CLASS& m)
{
    return os << to_string(m);
}
}  // namespace mio
